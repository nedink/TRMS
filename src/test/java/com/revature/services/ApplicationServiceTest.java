package com.revature.services;

import com.amazonaws.services.opsworks.model.App;
import com.revature.beans.*;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ApplicationServiceTest {

    private static ApplicationService applicationService = ApplicationService.getInstance();

//    @Test
//    public void newApplicationTest() {
//        Application application = new Application();
//        application.setApplicant(new Employee());
//        application.setAppStatus(new AppStatusType());
//        application.setEventGradeFormat(new GradeFormat());
//        application.setEventType(new EventType());
//        applicationService.newApplication(application);
//    }

    @Before
    public void setup() {
        new ApplyServiceTest().applyWithTest();
    }

    @Test
    public void ApplicationServiceTest() {
        ApplicationService result = ApplicationService.getInstance();
        assertNotNull(result);
    }

    @Test
    public void getApplicationByIDTest() {
        Application result = applicationService.getApplication(1);
        assertEquals(1, result.getId());
    }

    @Test
    public void getApplicationsByApplicantTest() {
        List<Application> result = applicationService.getApplicationsByApplicant(
                EmployeeService.getInstance().getEmployee("tprice"));
        assertNotNull(result);
    }

    @Test
    public void deleteAllApplicationsTest() {
        applicationService.deleteAllApplications();
        assertNull(applicationService.getApplication(1));
    }
}
