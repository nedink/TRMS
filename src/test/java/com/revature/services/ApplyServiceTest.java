package com.revature.services;

import com.revature.beans.Application;
import org.junit.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

import static org.junit.Assert.*;

public class ApplyServiceTest {

    private static ApplyService applyService = ApplyService.getInstance();

    @Test
    public void ApplyServiceTest() {
        ApplyService result = ApplyService.getInstance();
        assertNotNull(result);
    }

    @Test
    public void applyWithTest() {
        Application application = new Application();
        try {
            application = applyService.applyWith(
                    EmployeeService.getInstance().getEmployee("tprice"),
                    Date.valueOf("2018-12-17"),
                    Time.valueOf("08:00:00"),
                    "11730 Plaza America Dr #205, Reston, VA 20190",
                    "Revature Certification",
                    BigDecimal.valueOf(200.00),
                    GradeFormatService.getInstance().getGradeFormat("Graded"),
                    0.80f,
                    EventTypeService.getInstance().getEventType("CERT"),
                    "Gotta get certified, you feel me?",
                    0
            );
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        assertNotEquals(0, application.getId());
    }

}
