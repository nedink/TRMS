package com.revature.services;

import com.revature.beans.Employee;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class EmployeeServiceTest {

    private static EmployeeService employeeService = EmployeeService.getInstance();

    @Test
    public void EmployeeServiceTest() {
        EmployeeService result = EmployeeService.getInstance();
        assertNotNull(result);
    }

    @Test
    public void getEmployeeByIDTest() {
        Employee result = employeeService.getEmployee(1);
        assertNotNull(result);
    }

    @Test
    public void getEmployeeByUsernameTest() {
        Employee result = employeeService.getEmployee("dbryant");
        assertNotNull(result);
    }

    @Test
    public void getImmediateUnderlingsTest() {
        Employee employee = employeeService.getEmployee("dbryant");
        List<Employee> result = employeeService.getImmediateUnderlings(employee);
        assertNotNull(result);
    }

    @Test
    public void newEmployeeTest() {
    }
}
