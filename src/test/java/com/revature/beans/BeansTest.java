package com.revature.beans;

import net.sf.beanrunner.BeanRunner;
import org.junit.Test;

import java.sql.Date;
import java.sql.Time;

public class BeansTest {

    private BeanRunner beanRunner = new BeanRunner();

    private Application application = new Application();
    private AppStatusType appStatusType = new AppStatusType();
    private Department department = new Department();
    private Employee employee = new Employee();
    private EventType eventType = new EventType();
    private GradeFormat gradeFormat = new GradeFormat();
    private RoleType roleType = new RoleType();

    @Test
    public void testApplication() {
        beanRunner.addTestValue(Date.class, new Date(0));
        beanRunner.addTestValue(Time.class, new Time(0));
        try {
            beanRunner.testBean(application);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAppStatusType() {
        try {
            beanRunner.testBean(appStatusType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDepartment() {
        try {
            beanRunner.testBean(department);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEmployee() {
        try {
            beanRunner.testBean(employee);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testEventType() {
        try {
            beanRunner.testBean(eventType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGradeFormat() {
        try {
            beanRunner.testBean(gradeFormat);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testRoleType() {
        try {
            beanRunner.testBean(roleType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
