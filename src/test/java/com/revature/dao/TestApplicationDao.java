package com.revature.dao;

import com.revature.beans.Application;
import com.revature.util.ConnectionUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestApplicationDao {
    @Mock
    ConnectionUtil cu;

    @Mock
    Connection conn;

    @Mock
    PreparedStatement ps;

    @Mock
    ResultSet results;

    @Before
    public void setup() throws Exception {
        when(cu.getConnection()).thenReturn(conn);
        when(conn.prepareStatement(anyString())).thenReturn(ps);
        when(ps.executeQuery()).thenReturn(results);
        when(ps.executeUpdate()).thenReturn(0);
        when(results.next()).thenReturn(true).thenReturn(false);
        when(results.getInt(anyString())).thenReturn(1);
    }

//    @Test
//    public void testNewApplication() {
//        ApplicationDaoJDBC applicationDaoJDBC = new ApplicationDaoJDBC();
//        Application application = new Application();
//
//    }
}
