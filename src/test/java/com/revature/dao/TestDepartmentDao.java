package com.revature.dao;

import com.revature.beans.Department;
import com.revature.util.ConnectionUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDepartmentDao {
    @Mock
    ConnectionUtil cu;

    @Mock
    Connection conn;

    @Mock
    PreparedStatement ps;

    @Mock
    ResultSet results;

    @Before
    public void setup() throws Exception {
        when(cu.getConnection()).thenReturn(conn);
        when(conn.prepareStatement(anyString())).thenReturn(ps);
        when(ps.executeQuery()).thenReturn(results);
        when(ps.executeUpdate()).thenReturn(0);
        when(results.next()).thenReturn(true).thenReturn(false);
        when(results.getInt(anyString())).thenReturn(1);
    }

//    @Test
//    public void testGetDepartment() {
//        DepartmentDaoJDBC dao = new DepartmentDaoJDBC(cu);
//        Department department = dao.getByID(1);
//        assertNotEquals(0, department.getId());
//    }

//    @Test
//    public void testNewDepartment() {
//        DepartmentDaoJDBC dao = new DepartmentDaoJDBC(cu);
//        Department department = new Department();
//        dao.newDepartment(department);
//        assertNotEquals(0, department.getId());
//    }
}
