package com.revature.dao;

import com.revature.beans.Employee;
import com.revature.util.ConnectionUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;


@RunWith(MockitoJUnitRunner.class)
public class TestEmployeeDao {

    @Mock
    ConnectionUtil cu;

    @Mock
    Connection conn;

    @Mock
    PreparedStatement ps;

    @Mock
    ResultSet results;

    @Before
    public void setup() throws Exception {
        when(cu.getConnection()).thenReturn(conn);
        when(conn.prepareStatement(anyString())).thenReturn(ps);
        when(ps.executeQuery()).thenReturn(results);
        when(results.next()).thenReturn(true).thenReturn(false);
        when(results.getInt(anyString())).thenReturn(1);
    }

//    @Test
//    public void testGetEmployee() {
//        EmployeeDaoJDBC dao = new EmployeeDaoJDBC(cu);
//        Employee employee = new Employee();
//        employee.setUsername("username");
//        employee.setPassword("password");
//
//        dao.newEmployee(employee);
//        assertTrue(1 == employee.getId());
//    }

//    @Test
//    public void testSaveUser() throws Exception {
//        UserDao dao = new UserDao(cu);
//        User newUser = new User();
//        newUser.setUsername("username");
//        newUser.setPassword("password");
//        dao.saveUser(newUser);
//        assertTrue(1 == newUser.getId());
//    }

}
