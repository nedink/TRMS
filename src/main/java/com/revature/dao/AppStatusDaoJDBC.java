package com.revature.dao;

import com.revature.beans.AppStatusType;
import com.revature.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AppStatusDaoJDBC implements AppStatusDao {

    private static ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();
    private static AppStatusDaoJDBC instance = null;

    private AppStatusDaoJDBC () {
    }

    public static AppStatusDaoJDBC getInstance() {
        if (instance == null) instance = new AppStatusDaoJDBC ();
        return instance;
    }

    @Override
    public AppStatusType getByID(int id) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT status_id, status_name FROM app_status_types WHERE status_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, id);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                AppStatusType appStatusType = new AppStatusType();

                appStatusType.setId(results.getInt("status_id"));
                appStatusType.setName(results.getString("status_name"));

                return appStatusType;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public AppStatusType getByName(String name) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT status_id, status_name FROM app_status_types WHERE status_name = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setString(1, name);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                AppStatusType appStatusType = new AppStatusType();

                appStatusType.setId(results.getInt("status_id"));
                appStatusType.setName(results.getString("status_name"));

                return appStatusType;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getNameByID(int id) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT status_name FROM app_status_types WHERE status_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, id);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                return results.getString("status_name");
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getIDByName(String name) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT status_id FROM app_status_types WHERE status_name = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setString(1, name);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                return results.getInt("status_id");
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
