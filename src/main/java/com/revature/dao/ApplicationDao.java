package com.revature.dao;

import com.revature.beans.Application;
import com.revature.beans.Employee;

import java.math.BigDecimal;
import java.util.List;

public interface ApplicationDao {

    public Application getByID(int id);

    List<Application> getApplicationsByApplicant(Employee applicant);

    public int getUrgency(Application application);

    public int setUrgency(Application application, int urgency);

    public BigDecimal reimbursementThisYearFor(Employee applicant);

    public void newApplication(Application application);

    public void deleteAll(); // DO NOT USE
}
