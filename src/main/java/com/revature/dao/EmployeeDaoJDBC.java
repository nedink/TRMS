package com.revature.dao;

import com.revature.beans.Employee;
import com.revature.beans.RoleType;
import com.revature.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoJDBC implements EmployeeDao {

    private static ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();
    private static EmployeeDaoJDBC instance = null;

    private EmployeeDaoJDBC(){}

    public static EmployeeDaoJDBC getInstance() {
        if (instance == null) instance = new EmployeeDaoJDBC();
        return instance;
    }

    @Override
    public Employee getByID(int id) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery =
                    "SELECT emp_id, username, password, direct_super, emp_role, role_name, first_name, " +
                    "last_name, email, phone FROM employees JOIN role_types ON (emp_role = role_id) WHERE emp_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, id);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                Employee employee = new Employee();

                employee.setId(results.getInt("emp_id"));
                employee.setUsername(results.getString("username"));
                employee.setPassword(results.getString("password"));
                employee.setDirectSupervisor(getByID(results.getInt("direct_super")));

                RoleType roleType = new RoleType();
                roleType.setId(results.getInt("emp_role"));
                roleType.setName(results.getString("role_name"));
                employee.setRole(roleType);

                employee.setFirstName(results.getString("first_name"));
                employee.setLastName(results.getString("last_name"));
                employee.setEmail(results.getString("email"));
                employee.setPhone(results.getString("phone"));

                return employee;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getIDByUsername(String username) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT emp_id FROM employees WHERE username = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setString(1, username);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                return results.getInt("emp_id");
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Employee> getImmediateUnderlings(Employee employee) {

        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery =
                    "SELECT emp_id, username, password, direct_super, emp_role, role_name, first_name, " +
                    "last_name, email, phone FROM employees JOIN role_types ON (emp_role = role_id) " +
                            "WHERE direct_super = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, employee.getId());

            ResultSet results = ps.executeQuery();

            List<Employee> underlings = new ArrayList<>();
            while (results.next()) {
                Employee underling = new Employee();

                underling.setId(results.getInt("emp_id"));
                underling.setUsername(results.getString("username"));
                underling.setPassword(results.getString("password"));
                underling.setDirectSupervisor(getByID(results.getInt("direct_super")));

                RoleType roleType = new RoleType();
                roleType.setId(results.getInt("emp_role"));
                roleType.setName(results.getString("role_name"));
                underling.setRole(roleType);

                underling.setFirstName(results.getString("first_name"));
                underling.setLastName(results.getString("last_name"));
                underling.setEmail(results.getString("email"));
                underling.setPhone(results.getString("phone"));

                underlings.add(underling);
            }
            return underlings;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void newEmployee(Employee employee) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery =
                    "INSERT INTO employees (username, password, direct_super, emp_role, first_name, " +
                    "last_name, email, phone) " +
                            "VALUES (?, ?, ?, ?, ?, ?, ?, ?) RETURNING emp_id";
//            PreparedStatement ps = conn.prepareStatement(standardQuery, Statement.RETURN_GENERATED_KEYS);
            PreparedStatement ps = conn.prepareStatement(standardQuery);

            ps.setString(1, employee.getUsername());
            ps.setString(2, employee.getPassword());
            ps.setInt(3, employee.getDirectSupervisor() != null ?
                    employee.getDirectSupervisor().getId() : 1);
            ps.setInt(4, employee.getRole() != null ?
                    employee.getRole().getId() : 1);
            ps.setString(5,employee.getFirstName());
            ps.setString(6,employee.getLastName());
            ps.setString(7, employee.getEmail());
            ps.setString(8, employee.getPhone());

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                employee.setId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
