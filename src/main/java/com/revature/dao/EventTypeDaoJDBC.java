package com.revature.dao;

import com.revature.beans.EventType;
import com.revature.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EventTypeDaoJDBC implements EventTypeDao {

    private static ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();
    private static EventTypeDaoJDBC instance = null;

    private EventTypeDaoJDBC(){}

    public static EventTypeDaoJDBC getInstance() {
        if (instance == null) instance = new EventTypeDaoJDBC();
        return instance;
    }

    @Override
    public EventType getByID(int id) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT evt_id, evt_name, percent_covered FROM event_types WHERE evt_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, id);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                EventType eventType = new EventType();

                eventType.setId(resultSet.getInt("evt_id"));
                eventType.setName(resultSet.getString("evt_name"));
                eventType.setPercentCovered(resultSet.getFloat("percent_covered"));

                return eventType;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public EventType getByName(String name) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT evt_id, evt_name, percent_covered FROM event_types WHERE evt_name = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setString(1, name);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                EventType eventType = new EventType();
                eventType.setId(results.getInt("evt_id"));
                eventType.setName(results.getString("evt_name"));
                eventType.setPercentCovered(results.getFloat("percent_covered"));
                return eventType;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
