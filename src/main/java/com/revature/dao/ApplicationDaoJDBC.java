package com.revature.dao;

import com.revature.beans.*;
import com.revature.util.ConnectionUtil;

import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ApplicationDaoJDBC implements ApplicationDao {

    private static ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();
    private static ApplicationDaoJDBC instance = null;

    private ApplicationDaoJDBC() {
    }

    public static ApplicationDaoJDBC getInstance() {
        if (instance == null) {
            instance = new ApplicationDaoJDBC();
        }
        return instance;
    }

    @Override
    public Application getByID(int id) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery =
                    "SELECT app_id, applicant_id, app_status, status_name, date_submitted, evt_date, evt_time, " +
                            "evt_loc, evt_desc, evt_cost, grade_format, format_name, grade_cutoff, evt_type, " +
                            "evt_name, work_reason, urgency FROM applications " +
                            "JOIN app_status_types ON (app_status = status_id) " +
                            "JOIN grade_formats ON (grade_format = format_id) " +
                            "JOIN event_types ON (evt_type = evt_id) " +
                            "WHERE app_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, id);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                Application application = new Application();

                application.setId(results.getInt("app_id"));
                application.setApplicant(
                        EmployeeDaoJDBC.getInstance().getByID(results.getInt("applicant_id")));

                AppStatusType appStatusType = new AppStatusType();
                appStatusType.setId(results.getInt("app_status"));
                appStatusType.setName(results.getString("status_name"));
                application.setAppStatus(appStatusType);

                application.setDateSubmitted(results.getDate("date_submitted"));
                application.setEventDate(results.getDate("evt_date"));
                application.setEventTime(results.getTime("evt_time"));
                application.setEventLocation(results.getString("evt_loc"));
                application.setEventDescription(results.getString("evt_desc"));
                application.setEventCost(results.getBigDecimal("evt_cost"));

                GradeFormat gradeFormat = new GradeFormat();
                gradeFormat.setId(results.getInt("grade_format"));
                gradeFormat.setName(results.getString("format_name"));
                application.setEventGradeFormat(gradeFormat);

                application.setEventGradeCutoff(results.getFloat("grade_cutoff"));

                EventType eventType = new EventType();
                eventType.setId(results.getInt("evt_type"));
                eventType.setName(results.getString("evt_name"));
                application.setEventType(eventType);

                application.setUrgency(results.getInt("urgency"));

                return application;
            }
            return null;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Application> getApplicationsByApplicant(Employee applicant) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery =
                    "SELECT app_id, applicant_id, app_status, status_name, date_submitted, evt_date, evt_time, " +
                            "evt_loc, evt_desc, evt_cost, grade_format, format_name, grade_cutoff, evt_type, " +
                            "evt_name, work_reason, urgency FROM applications " +
                            "JOIN app_status_types ON (app_status = status_id) " +
                            "JOIN grade_formats ON (grade_format = format_id) " +
                            "JOIN event_types ON (evt_type = evt_id) " +
                            "WHERE app_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, applicant.getId());

            ResultSet results = ps.executeQuery();

            List<Application> applications = new ArrayList<>();
            while (results.next()) {
                Application application = new Application();

                application.setId(results.getInt("app_id"));
                application.setApplicant(
                        EmployeeDaoJDBC.getInstance().getByID(results.getInt("applicant_id")));

                AppStatusType appStatusType = new AppStatusType();
                appStatusType.setId(results.getInt("app_status"));
                appStatusType.setName(results.getString("status_name"));
                application.setAppStatus(appStatusType);

                application.setDateSubmitted(results.getDate("date_submitted"));
                application.setEventDate(results.getDate("evt_date"));
                application.setEventTime(results.getTime("evt_time"));
                application.setEventLocation(results.getString("evt_loc"));
                application.setEventDescription(results.getString("evt_desc"));
                application.setEventCost(results.getBigDecimal("evt_cost"));

                GradeFormat gradeFormat = new GradeFormat();
                gradeFormat.setId(results.getInt("grade_format"));
                gradeFormat.setName(results.getString("format_name"));
                application.setEventGradeFormat(gradeFormat);

                application.setEventGradeCutoff(results.getFloat("grade_cutoff"));

                EventType eventType = new EventType();
                eventType.setId(results.getInt("evt_type"));
                eventType.setName(results.getString("evt_name"));
                application.setEventType(eventType);

                application.setUrgency(results.getInt("urgency"));

                applications.add(application);
            }
            return applications;

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getUrgency(Application application) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT urgency FROM applications WHERE app_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, application.getId());

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt("urgency");
            }
            return -1;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public int setUrgency(Application application, int urgency) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "UPDATE applications SET urgency = ? FROM applications WHERE app_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, urgency);
            ps.setInt(2, application.getId());

            return ps.executeUpdate();

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public BigDecimal reimbursementThisYearFor(Employee applicant) {
        try (Connection conn = connectionUtil.getConnection()) {

            String query = "SELECT SUM(evt_cost) FROM applications WHERE (applicant_id = ?) " +
                    "AND (SUBSTRING(TEXT(date_submitted) FROM 1 FOR 4) = substring(TEXT(CURRENT_DATE) FROM 1 FOR 4))";
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, applicant.getId());

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                BigDecimal result = resultSet.getBigDecimal(1);
                result = result == null ? new BigDecimal(0) : result;
                return result.abs().equals(result) ? result : new BigDecimal(0); // if negative, make 0
            }
            else {
                return new BigDecimal(0);
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void newApplication(Application application) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "INSERT INTO applications (applicant_id, app_status, date_submitted, evt_date, evt_time, evt_loc, " +
                    "evt_desc, evt_cost, grade_format, grade_cutoff, evt_type, work_reason, urgency) " +
                    "VALUES (?, ?, CURRENT_DATE, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING app_id";
            PreparedStatement ps = conn.prepareStatement(standardQuery);

            ps.setInt(1, application.getApplicant().getId());
            ps.setInt(2, application.getAppStatus().getId());
            ps.setDate(3, application.getEventDate());
            ps.setTime(4, application.getEventTime());
            ps.setString(5, application.getEventLocation());
            ps.setString(6, application.getEventDescription());
            ps.setBigDecimal(7, application.getEventCost());
            ps.setInt(8, application.getEventGradeFormat().getId());
            ps.setFloat(9, application.getEventGradeCutoff());
            ps.setInt(10, application.getEventType().getId());
            ps.setString(11, application.getWorkReason());
            ps.setInt(12, application.getUrgency());

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                application.setId(resultSet.getInt(1));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAll() { // DO NOT USE
        try (Connection conn = connectionUtil.getConnection()) {
            String query = "DELETE FROM applications;ALTER SEQUENCE applications_app_id_seq RESTART WITH 1;";
            PreparedStatement ps = conn.prepareStatement(query);

            ps.executeUpdate();

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
