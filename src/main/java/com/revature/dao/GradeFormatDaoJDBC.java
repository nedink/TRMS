package com.revature.dao;

import com.revature.beans.GradeFormat;
import com.revature.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GradeFormatDaoJDBC implements GradeFormatDao {

    private static ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();
    private static GradeFormatDaoJDBC instance = null;

    private GradeFormatDaoJDBC(){}

    public static GradeFormatDaoJDBC getInstance() {
        if (instance == null) instance = new GradeFormatDaoJDBC();
        return instance;
    }

    @Override
    public GradeFormat getByID(int id) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT format_id, format_name FROM grade_formats WHERE format_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, id);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                GradeFormat gradeFormat = new GradeFormat();
                gradeFormat.setId(results.getInt("format_id"));
                gradeFormat.setName(results.getString("format_name"));
                return gradeFormat;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public GradeFormat getByName(String name) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT format_id, format_name FROM grade_formats WHERE format_name = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setString(1, name);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                GradeFormat gradeFormat = new GradeFormat();
                gradeFormat.setId(results.getInt("format_id"));
                gradeFormat.setName(results.getString("format_name"));
                return gradeFormat;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
