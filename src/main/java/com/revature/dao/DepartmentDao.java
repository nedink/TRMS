package com.revature.dao;

import com.revature.beans.Department;

public interface DepartmentDao {

    public Department getByID(int id);

    public int getIDByName(String name);

    public int getHead(int id);

    public void newDepartment(Department department);
}
