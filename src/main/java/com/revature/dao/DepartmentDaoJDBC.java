package com.revature.dao;

import com.revature.beans.Department;
import com.revature.beans.Employee;
import com.revature.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartmentDaoJDBC implements DepartmentDao {

    private static ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();
    private static DepartmentDaoJDBC instance = null;

    private DepartmentDaoJDBC(){}
    public static DepartmentDaoJDBC getInstance() {
        if (instance == null) instance = new DepartmentDaoJDBC();
        return instance;
    }

    @Override
    public Department getByID(int id) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT dep_name, dep_head FROM departments WHERE dep_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, id);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                Department department = new Department();
                department.setName(results.getString("dep_name"));
                Employee head = new Employee();
                department.setHead(EmployeeDaoJDBC.getInstance().getByID(results.getInt("dep_head")));

                return department;
            }
            else {
                return null;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getIDByName(String dep_name) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT dep_id FROM departments WHERE dep_name = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setString(1, dep_name);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                return results.getInt("dep_id");
            }
            else {
                return 0;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int getHead(int id) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "SELECT dep_head FROM departments WHERE dep_id = ?";
            PreparedStatement ps = conn.prepareStatement(standardQuery);
            ps.setInt(1, id);

            ResultSet results = ps.executeQuery();

            if (results.next()) {
                return results.getInt("dep_head");
            }
            else {
                return 0;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void newDepartment(Department department) {
        try (Connection conn = connectionUtil.getConnection()) {
            String standardQuery = "INSERT INTO departments (dep_name, dep_head) VALUES (?, ?) RETURNING dep_id";
            PreparedStatement ps = conn.prepareStatement(standardQuery);

            ps.setString(1, department.getName());
            ps.setInt(2, department.getHead().getId());

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                department.setId(resultSet.getInt(1));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
