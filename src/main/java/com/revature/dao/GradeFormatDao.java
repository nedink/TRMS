package com.revature.dao;

import com.revature.beans.GradeFormat;

public interface GradeFormatDao {

    public GradeFormat getByID(int id);

    public GradeFormat getByName(String name);
}
