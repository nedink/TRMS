package com.revature.dao;

import com.revature.beans.RoleType;
import com.revature.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleTypeDaoJDBC implements RoleTypeDao {

    private static ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();
    private static RoleTypeDaoJDBC instance = null;

    private RoleTypeDaoJDBC(){}

    public static RoleTypeDaoJDBC getInstance() {
        if (instance == null) instance = new RoleTypeDaoJDBC();
        return instance;
    }

    @Override
    public RoleType getByID(int id) {
        try (Connection conn = connectionUtil.getConnection()) {
            String query = "SELECT role_id, role_name FROM role_types WHERE role_id = ?;";
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setInt(1, id);

            ResultSet resultSet = ps.executeQuery();

            if (resultSet.next()) {
                RoleType roleType = new RoleType();

                roleType.setId(resultSet.getInt("role_id"));
                roleType.setName(resultSet.getString("role_name"));

                return roleType;
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
