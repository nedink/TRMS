package com.revature.dao;

import com.revature.beans.Employee;

import java.util.List;

public interface EmployeeDao {

    public Employee getByID(int id);

    public int getIDByUsername(String username);

    public List<Employee> getImmediateUnderlings(Employee employee);

    public void newEmployee(Employee employee);
}
