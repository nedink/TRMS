package com.revature.dao;

import com.revature.beans.EventType;

public interface EventTypeDao {

    public EventType getByID(int id);

    public EventType getByName(String name);
}
