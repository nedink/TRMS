package com.revature.dao;

import com.revature.beans.RoleType;

public interface RoleTypeDao {

    public RoleType getByID(int id);
}
