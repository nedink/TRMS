package com.revature.dao;

import com.revature.beans.AppStatusType;

public interface AppStatusDao {

    public AppStatusType getByID(int id);

    public AppStatusType getByName(String name);

    public String getNameByID(int id);

    public int getIDByName(String name);
}
