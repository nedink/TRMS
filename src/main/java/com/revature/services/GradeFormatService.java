package com.revature.services;

import com.revature.beans.GradeFormat;
import com.revature.dao.GradeFormatDaoJDBC;

public class GradeFormatService {

    private static GradeFormatService instance = null;

    private GradeFormatService () {
    }

    public static GradeFormatService getInstance() {
        if (instance == null) {
            instance = new GradeFormatService ();
        }
        return instance;
    }

    public GradeFormat getGradeFormat(int id) {
        return GradeFormatDaoJDBC.getInstance().getByID(id);
    }

    public GradeFormat getGradeFormat(String name) {
        return GradeFormatDaoJDBC.getInstance().getByName(name);
    }
}
