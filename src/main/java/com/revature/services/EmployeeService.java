package com.revature.services;

import com.revature.beans.Employee;
import com.revature.dao.EmployeeDaoJDBC;

import java.util.ArrayList;
import java.util.List;

public class EmployeeService extends LowLevelService {

    private static EmployeeService instance = null;

    private EmployeeService() {
    }

    public static EmployeeService getInstance() {
        if (instance == null) {
            instance = new EmployeeService();
        }
        return instance;
    }

    public Employee getEmployee(int id) {
        return EmployeeDaoJDBC.getInstance().getByID(id);
    }

    public Employee getEmployee(String name) {
        return getEmployee(EmployeeDaoJDBC.getInstance().getIDByUsername(name));
    }

    public List<Employee> getImmediateUnderlings(Employee employee) {
        return EmployeeDaoJDBC.getInstance().getImmediateUnderlings(employee);
    }

    public void newEmployee(Employee employee) {
        EmployeeDaoJDBC.getInstance().newEmployee(employee);
    }
}
