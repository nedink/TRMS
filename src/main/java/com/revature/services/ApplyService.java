package com.revature.services;

import com.revature.beans.*;
import com.revature.dao.AppStatusDao;
import com.revature.dao.AppStatusDaoJDBC;
import com.revature.dao.ApplicationDao;
import com.revature.dao.ApplicationDaoJDBC;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

// High Level
public class ApplyService extends HighLevelService {

    private static ApplyService instance = null;
    private ApplyService(){}
    public static ApplyService getInstance() {
        if (instance == null) instance = new ApplyService();
        return instance;
    }

    // returns an int representing an error code
    public Application applyWith(Employee applicant, Date evt_date, Time evt_time, String evt_loc,
                         String evt_desc, BigDecimal evt_cost, GradeFormat grade_format, float grade_cutoff,
                         EventType evt_type, String work_reason, int urgency) throws Exception {

        Application application = new Application();
        application.setApplicant(applicant);

        switch (applicant.getRole().getName()) {
            // set app status based on role of applicant
            case "NONE": {
                application.setAppStatus(AppStatusService.getInstance().getByName("UPTO_DIRSUPER"));
            } break;
            case "DIRSUPER": {
                application.setAppStatus(AppStatusService.getInstance().getByName("UPTO_DEPHEAD"));
            } break;
            case "DEPHEAD": {
                application.setAppStatus(AppStatusService.getInstance().getByName("UPTO_BENCO"));
            } break;
            default: {
                throw new Exception("Applicant role invalid");
            }
        }
        application.setEventDate(evt_date);
        application.setEventTime(evt_time);
        application.setEventLocation(evt_loc);
        application.setEventDescription(evt_desc);
        application.setEventCost(evt_cost);
        application.setEventGradeFormat(grade_format);
        application.setEventGradeCutoff(grade_cutoff);
        application.setEventType(evt_type);
        application.setWorkReason(work_reason);
        application.setUrgency(urgency);

        // TODO: Check date/time for eligibility/urgency

        // TODO: Check reimbursement amount against available amount

        ApplicationService.getInstance().newApplication(application);

        return application;
    }
}
