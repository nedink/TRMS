package com.revature.services;

import com.revature.beans.*;
import com.revature.dao.*;

import java.math.BigDecimal;
import java.util.List;

public class ApplicationService extends LowLevelService {

    private static ApplicationService instance = null;

    private ApplicationService() {
    }

    public static ApplicationService getInstance() {
        if (instance == null) instance = new ApplicationService();
        return instance;
    }

    public Application getApplication(int id) {
        return ApplicationDaoJDBC.getInstance().getByID(id);
    }

    public List<Application> getApplicationsByApplicant(Employee applicant) {
        return ApplicationDaoJDBC.getInstance().getApplicationsByApplicant(applicant);
    }

    public int getUrgency(Application application) {
        return ApplicationDaoJDBC.getInstance().getUrgency(application);
    }

    public int setUrgency(Application application, int urgency) {
        return ApplicationDaoJDBC.getInstance().setUrgency(application, urgency);
    }

    public BigDecimal getReimbursementAvailableFor(Employee applicant) {
        // TODO: value [1000] should be moved to database
        BigDecimal yearlyAllowance = new BigDecimal(1000.00);
        BigDecimal spentThisYear = ApplicationDaoJDBC.getInstance().reimbursementThisYearFor(applicant);
        return yearlyAllowance.subtract(spentThisYear);
    }

    public void newApplication(Application application) {
        ApplicationDaoJDBC.getInstance().newApplication(application);
    }

    public void deleteAllApplications() {
        ApplicationDaoJDBC.getInstance().deleteAll();
    }
}
