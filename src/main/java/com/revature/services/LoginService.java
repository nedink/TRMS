package com.revature.services;

public class LoginService extends HighLevelService {
    private static LoginService instance = null;

    private LoginService() {
    }

    public static LoginService getInstance() {
        if (instance == null) {
            instance = new LoginService();
        }
        return instance;
    }

}
