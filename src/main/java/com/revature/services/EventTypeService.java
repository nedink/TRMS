package com.revature.services;

import com.revature.beans.EventType;
import com.revature.dao.EventTypeDaoJDBC;

public class EventTypeService extends LowLevelService {

    private static EventTypeService instance = null;

    private EventTypeService () {
    }

    public static EventTypeService getInstance() {
        if (instance == null) {
            instance = new EventTypeService ();
        }
        return instance;
    }

    public EventType getEventType(int id) {
        return EventTypeDaoJDBC.getInstance().getByID(id);
    }

    public EventType getEventType(String name) {
        return EventTypeDaoJDBC.getInstance().getByName(name);
    }
}
