package com.revature.services;

import com.revature.beans.Application;
import com.revature.beans.Employee;

import java.util.ArrayList;
import java.util.List;

public class ReviewService extends HighLevelService {

    private static ReviewService instance = null;

    private ReviewService () {
    }

    public static ReviewService getInstance() {
        if (instance == null) {
            instance = new ReviewService ();
        }
        return instance;
    }

    private List<Employee> getAllUnderlings(Employee employee) {
        List<Employee> underlings = new ArrayList<>();
        for (Employee e : EmployeeService.getInstance().getImmediateUnderlings(employee)) {
            underlings.add(e);
            underlings.addAll(getAllUnderlings(e));
        }
        return underlings;
    }

    public List<Application> getApplicationsForReview(Employee employee) {

        String appStatusRequested = null; // not a real app status
        switch (employee.getRole().getName()) {
            case "NONE": {
                // no app status requested; employee role 'none'
            } break;
            case "DIRSUPER": {
                appStatusRequested = "UPTO_DIRSUPER";
            } break;
            case "DEPHEAD": {
                appStatusRequested = "UPTO_DEPHEAD";
            } break;
            case "BENCO": {
                appStatusRequested = "UPTO_BENCO";
            } break;
            case "EXEC": {
                // no app status requested; employee rank 'exec'
            } break;
        }

        List<Application> applications = new ArrayList<Application>();
        List<Employee> underlings = getAllUnderlings(employee);
        for (Employee e : underlings) {
            // for each underling, get his/her applications
            for (Application application : ApplicationService.getInstance().getApplicationsByApplicant(e)) {
                if (application.getAppStatus().getName().equals(appStatusRequested))
                    applications.add(application);
            }
        }

        return applications;
    }
}
