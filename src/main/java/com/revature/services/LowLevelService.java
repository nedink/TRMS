package com.revature.services;

/**
 * Communication with dao objects allowed.
 * Communication with other services disallowed.
 */
public abstract class LowLevelService extends Service {
}
