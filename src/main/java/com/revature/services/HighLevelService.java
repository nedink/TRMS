package com.revature.services;

/**
 * Communication with dao objects disallowed.
 * Communication with other services allowed.
 */
public abstract class HighLevelService extends Service {
}
