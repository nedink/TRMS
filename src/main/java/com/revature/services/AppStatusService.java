package com.revature.services;

import com.revature.beans.AppStatusType;
import com.revature.beans.Application;
import com.revature.dao.AppStatusDaoJDBC;

public class AppStatusService extends LowLevelService {

    private static AppStatusService instance = null;

    private AppStatusService() {
    }

    public static AppStatusService getInstance() {
        if (instance == null) instance = new AppStatusService();
        return instance;
    }

    public AppStatusType getById(int id) {
        return AppStatusDaoJDBC.getInstance().getByID(id);
    }

    public AppStatusType getByName(String name) {
        return AppStatusDaoJDBC.getInstance().getByName(name);
    }
}
