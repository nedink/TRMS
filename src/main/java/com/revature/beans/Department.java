package com.revature.beans;

public class Department {
    private int id;
    private String name;
    private Employee head;

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public Employee getHead() {
        return head;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setHead(Employee head) {
        this.head = head;
    }
}
