package com.revature.beans;

public class EventType extends TRMSType {
    private float percentCovered;

    public float getPercentCovered() {
        return percentCovered;
    }

    public void setPercentCovered(float percentCovered) {
        this.percentCovered = percentCovered;
    }
}
