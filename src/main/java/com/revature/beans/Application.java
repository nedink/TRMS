package com.revature.beans;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

public class Application {
    private int id;
    private Employee applicant;
    private AppStatusType appStatus;
    private Date dateSubmitted;

    private Date eventDate;
    private Time eventTime;
    private String eventLocation;
    private String eventDescription;
    private BigDecimal eventCost;
    private GradeFormat eventGradeFormat;
    private float eventGradeCutoff;
    private EventType eventType;
    private String workReason;
    private int urgency;

    public int getId() {
        return id;
    }
    public Employee getApplicant() {
        return applicant;
    }
    public AppStatusType getAppStatus() {
        return appStatus;
    }
    public Date getDateSubmitted() {
        return dateSubmitted;
    }
    public Date getEventDate() {
        return eventDate;
    }
    public Time getEventTime() {
        return eventTime;
    }
    public String getEventLocation() {
        return eventLocation;
    }
    public String getEventDescription() {
        return eventDescription;
    }
    public BigDecimal getEventCost() {
        return eventCost;
    }
    public GradeFormat getEventGradeFormat() {
        return eventGradeFormat;
    }
    public float getEventGradeCutoff() {
        return eventGradeCutoff;
    }
    public EventType getEventType() {
        return eventType;
    }
    public String getWorkReason() {
        return workReason;
    }
    public int getUrgency() {
        return urgency;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setApplicant(Employee applicant) {
        this.applicant = applicant;
    }
    public void setAppStatus(AppStatusType appStatus) {
        this.appStatus = appStatus;
    }
    public void setDateSubmitted(Date dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }
    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }
    public void setEventTime(Time eventTime) {
        this.eventTime = eventTime;
    }
    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }
    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }
    public void setEventCost(BigDecimal eventCost) {
        this.eventCost = eventCost;
    }
    public void setEventGradeFormat(GradeFormat gradeFormat) {
        this.eventGradeFormat = gradeFormat;
    }
    public void setEventGradeCutoff(float eventGradeCutoff) {
        this.eventGradeCutoff = eventGradeCutoff;
    }
    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
    public void setWorkReason(String workReason) {
        this.workReason = workReason;
    }
    public void setUrgency(int urgency) {
        this.urgency = urgency;
    }
}
