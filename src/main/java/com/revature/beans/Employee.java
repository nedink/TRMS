package com.revature.beans;

public class Employee {
    private int id;
    private String username;
    private String password;
    private Employee directSupervisor;
    private RoleType role;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;

    public int getId() {
        return id;
    }
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
    public Employee getDirectSupervisor() {
        return directSupervisor;
    }
    public RoleType getRole() {
        return role;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getEmail() {
        return email;
    }
    public String getPhone() {
        return phone;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setDirectSupervisor(Employee directSupervisor) {
        this.directSupervisor = directSupervisor;
    }
    public void setRole(RoleType role) {
        this.role = role;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
}
